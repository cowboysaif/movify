### What is movify ? 
Create a plugin that displays a list of movies, with the following tasks:  

A simple plugin that displays list of movies from custom post in frontpage. The plugin uses json api to display movies related information from the custom post.

### Installation:
1.	Upload the zip file to wordpress plugin directory (wp-content\plugins\)
2.	Unzip the file
    Or,
1.	Upload the zip file in the plugins>add new > upload section
2.	Click on Browse… and select ok
3.	Your movify plugin is ready to go !
###How to use 
Movify works like your regular posts in wordperss. If you are familiar with wordpress post, you are already familiar with movify !

1.	Go to Movify section and click on “Add new”
2.	Fill up Movie title, poster , description , rating and year
3.	Click publish. Your movie is ready to go in frontpage !
### Manage movies
Managing movies in movify is like managing posts. If you go to Movify->all movies section , you can see your movie posts, with ratings and descriptions.

You can add, delete or even quickly trash the posts, just like posts ! This section is properly tailored so that movie relevant information are shown with priority.