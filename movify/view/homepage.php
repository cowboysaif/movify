<?php
class homepage{
	public function __construct(){
		
	}
	
	public function movify_inject_html ( $content ) {
    if ( is_home() || is_front_page() ) {
	
		$json_api = new json_api();
		if ($json_api->send_json() == false  && !file_exists(WP_PLUGIN_DIR . '/movify/cache/cache.txt')) {
			echo '<p class="info">Oops ! no movies found ! </p>';
		
		}
		else {
			wp_register_style( 'frontendcss', plugins_url('movify/view/css/frontend.css') );
			wp_enqueue_style( 'frontendcss' );
			$string_data = file_get_contents(WP_PLUGIN_DIR . '/movify/cache/cache.txt');
			$array =  unserialize($string_data);
		
			echo '
			<div id="wrapper">
			<ul id="movieposters" style="font: 1.7 rem;">';
			for ( $i = 0 ; $i < sizeof ( $array['data'] ) ; $i++ ) {
				echo '<li>
				<img style="max-height:267px;" src="'.$array['data'][$i]['poster_url'].'" alt="Iron Man 2" />
				<div class="movieinfo" >
					<h3 style="font-size: large;">'.$array['data'][$i]['title'].'</h3>
					<p style="margin-left: 15px">'.substr($array['data'][$i]['short_description'],0,60).'...</p>
					<p style="margin-left: 15px">Rating : '.$array['data'][$i]['rating'].'/5 Year: '.$array['data'][$i]['year'].'</p>
					<a href="'.get_permalink($array['data'][$i]['id']).'">More info</a>
				</div>
			</li>';
			}
			echo '</ul></div>';
		}
		}
  
	}
}
?>