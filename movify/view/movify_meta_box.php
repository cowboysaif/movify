<?php
$description = '
			<div class="inside">
			<label class="screen-reader-text" for="excerpt">Description</label><textarea rows="1" cols="40" name="excerpt" id="excerpt">'.$post->post_excerpt.'</textarea>
			</div>
			</div>
			<div id="slugdiv" class="postbox  hide-if-js">
			<div class="inside">
			<label class="screen-reader-text" for="post_name">Slug</label><input name="post_name" type="text" size="13" id="post_name" value="'.$post->post_title.'">
			</div>';

$rating = get_post_meta( $post->ID, 'mvf_rating', true );
$year = get_post_meta( $post->ID, 'mvf_year', true );
$rating_year = '
			<div class="inside">
			<label class="screen-reader-text" for="rating_year">Rating and year</label>
			<p><b>Rating :</b></p>
			<div class="stars">
			<input type="radio" name="star" class="star-1" ';
			if ($rating == "1" ) $rating_year .= 'checked="checked";';
			$rating_year .= 'id="star-1" value="1"/>
			<label class="star-1" for="star-1">1</label>
			<input type="radio" name="star" class="star-2" ';
			if ($rating == "2" ) $rating_year .= 'checked="checked";';
			$rating_year .= 'id="star-2" value="2"/>
			<label class="star-2" for="star-2">2</label>
			<input type="radio" name="star" class="star-3" ';
			if ($rating == "3" ) $rating_year .= 'checked="checked";';
			$rating_year .= 'id="star-3" value="3"/>
			<label class="star-3" for="star-3">3</label>
			<input type="radio" name="star" class="star-4" ';
			if ($rating == "4" ) $rating_year .= 'checked="checked";';
			$rating_year .= 'id="star-4" value="4"/>
			<label class="star-4" for="star-4">4</label>
			<input type="radio" name="star" class="star-5" ';
			if ($rating == "5" ) $rating_year .= 'checked="checked";';
			$rating_year .= 'id="star-5" value="5"/>
			<label class="star-5" for="star-5">5</label>
			<span></span>
    		</div>
			<div style="padding-left: 183px; margin-top: -61px; margin-bottom: 32px;">
			<b>Year: <b>
			<p><input type="number" name="year" min="1600" max="2099" value="'.$year.'"></p>
			</div>	
			<div id="slugdiv" class="postbox  hide-if-js">
			<div class="inside">
			<label class="screen-reader-text" for="post_name">Slug</label><input name="post_name" type="text" size="13" id="post_name" value="'.$post->post_title.'">
			</div>';
?>