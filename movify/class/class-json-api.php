<?php
/*
Plugin name : Movify
Class name: json_api
Date: 01/10/15
Description : responsible for generating json request for front page
*/

class json_api {
	public function __construct(){
		
	}
	//generate json accoring to number of posts
	//in the data
	public function generate_json_movify($num) {
	
		$loop = new WP_Query( array( 'post_type' => 'movify'));
		$i = 1;
		$json = array();
		if ( $loop->have_posts() ) :
        while ( $loop->have_posts() ) : $loop->the_post();
		if ( $i > $num ) break;
			$json[]= array (
			'id' => get_the_ID(),
			'title' => get_the_title(),
			'poster_url' => get_post_meta( get_the_ID(), 'mvf_poster', true ),
			'rating' => get_post_meta( get_the_ID(), 'mvf_rating', true ),
			'year' => get_post_meta( get_the_ID(), 'mvf_year', true ),
			'short_description' => get_post_meta( get_the_ID(), 'mvf_description', true )
			);
			$i++;
		endwhile;
		endif;
		return $json;
	}
	
		/**
	 * @param string $url  Url for for the request
	 * @param string $data JSON-encoded data to send
	 */
	public function curl_json($url, $data)
	{
		$ch = curl_init($url);
		curl_setopt($ch, CURLOPT_CUSTOMREQUEST, "PUT"); // PUT HTTP method
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		curl_setopt($ch, CURLOPT_HTTPHEADER, array(
			'Content-Type: application/json',           // JSON content type
			'Content-Length: ' . strlen($data))
		);
		return curl_exec($ch);
	}
	
	public function send_json() {
	
		$ajax_url = admin_url('admin-ajax.php?action=json-movify');
		$date = $this->check_cache();
		
		$data = array( 'date' => $date );
		
		$json = json_encode($data);
		$json_response = $this->curl_json($ajax_url, $json);
		$p = json_decode($json_response, true);
		if ( $p != 0 ) {
		$this->save_cache($p);
		
		return $p;
		}
		return false;  
		
	}
	
	public function receive_json() {
	
			
		// Retrieve JSON payload
		$data = json_decode(file_get_contents('php://input'),true);
		$new_data = $this->check_update($data["date"]) ;
		
		if ( $new_data == false ) {

		}
		else {
		wp_send_json( array( 'last_updated' => $new_data ,
							 'data' => $this->generate_json_movify('9')
							 ));
		}
	}
	
	public function check_cache() {
		if ( !file_exists(WP_PLUGIN_DIR . '/movify/cache/cache.txt')) return false;
		else {
		$string_data = file_get_contents(WP_PLUGIN_DIR . '/movify/cache/cache.txt');
		$array =  unserialize($string_data);

		return $array["last_updated"];
		}
	}
	
	public function save_cache($data) {
		$string_data = serialize($data);
		file_put_contents(WP_PLUGIN_DIR . '/movify/cache/cache.txt', $string_data);
		
	}

	public function check_update($date) {

		$latest = new WP_Query( array( 'post_type' => 'movify'));

		if($latest->have_posts()){
			$modified_date = $latest->posts[0]->post_modified;
		}
		
		$lastdate = $modified_date;
		if ( $date  == false ) return strtotime($lastdate);
		elseif ( strcmp($date,$lastdate) == 0 ) return false;
		else if ( strtotime($date) > strtotime($lastpost)) return $lastdate;
		else return false ;
	}
	
	public function remove_cache() {
	
		unlink(WP_PLUGIN_DIR . '/movify/cache/cache.txt');
	}

}