<?php
/*
Plugin name : Movify
Class name: register_movify_post_add_metabox
Date: 27/9/15
Description : responsible for registering movie posts, adding metabox, adding metadata
*/
class register_movify_post_add_metabox {
	public function __construct(){
		
	}
	
	public function register_movie_post_type() {
	$labels = array(
		'name'               => _x( 'Movify', 'movify', 'mvf' ),
		'singular_name'      => _x( 'Movie', 'movie', 'mvf' ),
		'menu_name'          => _x( 'Movify', 'admin menu', 'mvf' ),
		'name_admin_bar'     => _x( 'Movie', 'add new on admin bar', 'mvf' ),
		'add_new'            => _x( 'Add New', 'Movie', 'mvf' ),
		'add_new_item'       => __( 'Add New Movie', 'mvf' ),
		'new_item'           => __( 'New Movie', 'mvf' ),
		'edit_item'          => __( 'Edit Movie', 'mvf' ),
		'view_item'          => __( 'View Movie', 'mvf' ),
		'all_items'          => __( 'All Movies', 'mvf' ),
		'search_items'       => __( 'Search Movies', 'mvf' ),
		'parent_item_colon'  => __( 'Parent Movies:', 'mvf' ),
		'not_found'          => __( 'No movies found.', 'mvf' ),
		'not_found_in_trash' => __( 'No movies found in Trash.', 'mvf' )
	);

	$args = array(
		'labels'             => $labels,
                'description'        => __( 'Moviefy movie post.', 'mvf' ),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'movie' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => false,
		'menu_position'      => null,
		'menu_icon' 		 => 'dashicons-editor-video',
		'supports'           => array( 'title', 'editor', 'thumbnail' , 'excerpt' )
	);

	register_post_type( 'movify', $args );
	
	}
	
	public function change_metaboxes() {
	//converting excerpt to description. 
	//leter on excerpt will be saved to meta data
		
		remove_meta_box( 'postexcerpt' , 'movify' , 'normal' ); 
		add_meta_box(
			'movify_description',
			__( 'Description', 'mvf' ),
			array($this, 'mvf_meta_box_description_callback'),
			'movify'
		);
		
		add_meta_box(
			'movify_rating_year',
			__( 'Rating and year', 'mvf' ),
			array($this, 'mvf_meta_box_rating_year_callback'),
			'movify'
		);
	}
	
	public function mvf_meta_box_description_callback() {
		global $post;
		include('/../view/movify_meta_box.php');
		echo $description;
	}
	
	public function mvf_meta_box_rating_year_callback() {
	wp_nonce_field( 'movify_save_meta_box_data', 'movify_meta_box_nonce' );
		global $post;
		wp_enqueue_style( 'backendcss' );
		include('/../view/movify_meta_box.php');
		echo $rating_year;
	}
	
	public function mvf_save_meta_box_data($post_id) {
		global $post;
		//Check if our nonce is set.
		if ( ! isset( $_POST['movify_meta_box_nonce'] ) ) {
			return;
		}
	
		// Verify that the nonce is valid.
		if ( ! wp_verify_nonce( $_POST['movify_meta_box_nonce'], 'movify_save_meta_box_data' ) ) {
			return;
		}
	
		// If this is an autosave, our form has not been submitted, so we don't want to do anything.
		if ( defined( 'DOING_AUTOSAVE' ) && DOING_AUTOSAVE ) {
			return;
		}
	
		// Check the user's permissions.
		if ( ! current_user_can( 'edit_post', $post_id ) ) {
			return;
		}
		/* OK, it's safe for us to save the data now. */

		//lets save excerpt data to meta data first
		update_post_meta( $post_id, 'mvf_description', $post->post_excerpt );
		//save feature image to metadata
		$id = get_post_thumbnail_id($post->ID);
		$src = wp_get_attachment_image_src( $id, 'movify');
		update_post_meta( $post_id, 'mvf_poster', $src[0]  );
		//other meta data from post 
		if (  isset( $_POST['star'] ) ) {
		// Sanitize user inputs.
		$star = sanitize_text_field( $_POST['star'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, 'mvf_rating', $star );
		}
		
		if (  isset( $_POST['year'] ) ) {
		// Sanitize user inputs.
		$year = sanitize_text_field( $_POST['year'] );

		// Update the meta field in the database.
		update_post_meta( $post_id, 'mvf_year', $year );
		
		}
	
	}
	
	
	public function add_backend_stylesheet() {
		wp_register_style( 'backendcss', plugins_url('movify/view/css/backend.css') );
		wp_register_style( 'frontendcss', plugins_url('movify/view/css/frontend.css') );
	}

	
}
?>