<?php 
/*
Plugin name : Movify
Class name: register_movify_post_add_metabox
Date: 29/9/15
Description : responsible for chanigng movify column
*/
class change_movify_column {
	public function __construct(){
		
	}

// change coloumns of movify list
	public function change_movify_columns($columns) {
	
		unset(
			$columns['date'],
			$columns['title']
		);
		
		$new_columns = array(
		'Movie' => __('Movie'),
		'Rating' => __('Rating'),
		'Year' => __('Year'),
		'Description' => __('Description')
		
		);
		return array_merge($columns, $new_columns );
	}
// manage columns
	public function manage_movify_columns( $column, $post_id ) {
	global $post;
		switch($column) {
			case 'Movie':
				$image = get_the_post_thumbnail($post_id , 'thumbnail',array('style'=>'display: block; 
								   max-width:100px; 
								   max-height:100px; 
								   width: auto;
    							   height: auto;'));
				echo $image;
				include('/../view/movify_column.php');
				echo $movie;
				
				break;
			case 'Rating':
				wp_enqueue_style( 'backendcss' );
				$rating = get_post_meta( $post->ID, 'mvf_rating', true );
				include('/../view/movify_column.php');
				echo $rating_text;
				break;
			case 'Year':
				echo get_post_meta( $post->ID, 'mvf_year', true );
				break;
			case 'Description':
				echo get_post_meta( $post->ID, 'mvf_description', true );
				break;
		}	
	}
}
	
?>