<?php

/**
 * Custom Featured Image Metabox.
 *
 * @package   Custom_Featured_Image_Metabox_Admin
 * @author    1fixdotio <1fixdotio@gmail.com>
 * @license   GPL-2.0+
 * @link      http://1fix.io
 * @copyright 2014 1Fix.io
 */

/**
 * Plugin class. This class should ideally be used to work with the
 * administrative side of the WordPress site.
 *
 * If you're interested in introducing public-facing
 * functionality, then refer to `class-custom-featured-image-metabox.php`
 *
 * @package Custom_Featured_Image_Metabox_Admin
 * @author  1fixdotio <1fixdotio>
 */
class Custom_Featured_Image_Metabox_Admin {



	/**
	 * Change the title of Featured Image Metabox
	 *
	 * @return null
	 *
	 * @since 0.8.0
	 */
	public function change_metabox_title($post_type) {

		//$post_type = $this->get_post_type();

		
			//remove original featured image metabox
			remove_meta_box( 'postimagediv', 'movify' , 'side' );

			//add our customized metabox
			add_meta_box( 'postimagediv', 'Poster' , 'post_thumbnail_meta_box', 'movify' , 'side', 'low' );
		

	} // end change_metabox_title

	/**
	 * Change metabox content
	 *
	 * @param  string $content HTML string
	 * @return string Modified content
	 *
	 * @since 0.8.0
	 */
	public function change_metabox_content( $content ) {



	
			$content = str_replace( __( 'Set featured image' ), 'Set Poster', $content );
		


			$content = str_replace( __( 'Remove featured image' ), 'Remove Poster', $content );
		

		return $content;

	} // end change_metabox_content

	/**
	 * Change the strings in media manager
	 *
	 * @param  array $strings Strings array
	 * @param  object $post   Post object
	 * @return array          Modified strings array
	 *
	 * @since 0.8.0
	 */
	public function change_media_strings( $strings, $post ) {

		if ( is_object( $post ) && ! empty( $post ) ) {
			$post_type = $post->post_type;
			$options = get_option( $this->plugin_slug . '_' . $post_type );

			if ( isset( $options['set_text'] ) && ! empty( $options['set_text'] ) ) {
				$strings['setFeaturedImage']      = $options['set_text'];
				$strings['setFeaturedImageTitle'] = $options['set_text'];
			}

		}

		return $strings;

	} // end change_media_strings

}
