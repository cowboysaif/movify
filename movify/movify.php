<?php 
	/*
	Plugin Name: Movify
	Plugin URI: http://moxienyc.workable.com/
	Description: A simple plugin that displays list of movies from custom post in frontpage. The plugin uses json api to display movies related information from the custom post.
	Author: Saif Taifur Anwar
	Version: 0.1
	Author URI: mailto:cowboysaif@gmail.com
	*/
define( 'WP_DEBUG', true );
define( 'WP_DEBUG_LOG', true );
define( 'WP_DEBUG_DISPLAY', false );
@ini_set( 'display_errors', 0 );

include('class/class-register-movify-post-add-metabox.php');
include('class/class-change-movify-column.php');
include('class/class-json-api.php');
include('view/homepage.php');
//third party library. gpl 2.0
include('gpl_libs/class-custom-featured-image-metabox-admin.php');	
	
class movify {
	public function __construct(){
	
		$register = new register_movify_post_add_metabox();
		$change = new change_movify_column();
		$poster = new Custom_Featured_Image_Metabox_Admin();
		$json = new json_api();
		$homepage = new homepage();
		//save error in case the plugin throws some error in activation
		add_action('activated_plugin',array( $movify, 'movify_save_error' ));
		//register movie posts
		add_action( 'init', array( $register, 'register_movie_post_type' ));
		//changing columns in movify 
		add_filter('manage_movify_posts_columns' , array( $change,'change_movify_columns'));
		// managing colums
		add_action( 'manage_movify_posts_custom_column', array( $change,'manage_movify_columns'), 10, 2 );
		// adding meta boxes to movify posts
		add_action( 'add_meta_boxes', array( $register,'change_metaboxes') );
		// add css stylesheet in movify post eidtor
		add_action( 'admin_init', array( $register,'add_backend_stylesheet') );
		//save meta box data
		add_action( 'save_post', array($register,'mvf_save_meta_box_data' ));

		add_action( 'add_meta_boxes', array( $poster, 'change_metabox_title' ) );
		add_filter( 'admin_post_thumbnail_html', array( $poster, 'change_metabox_content' ) );
		//send json and receive data
		add_action('wp_footer',array($json, 'send_json'));
		add_action('wp_ajax_json-movify', array($json, 'receive_json'));
		add_action('wp_ajax_nopriv_json-movify', array($json, 'receive_json'));
		//this time ... this time
		add_action('save_post' , array($json, 'remove_cache'));
		//add content to homepage
		add_filter( 'the_content',array($homepage, 'movify_inject_html'));
	}
	
	public function movify_save_error(){
		//save it in as a wp option value so that it can be accessed later on.
		//for debug purpose only.
    	update_option('plugin_error',  ob_get_contents());
    }

}

$movify = new movify();


?>